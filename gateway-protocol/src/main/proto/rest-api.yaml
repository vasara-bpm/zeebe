openapi: "3.1.0"
info:
  title: Zeebe REST API
  version: "0.1"
  description: API for communicating with the Zeebe cluster.
  license:
    name: Zeebe Community License Version 1.1
    url: https://github.com/camunda/zeebe/blob/main/licenses/ZEEBE-COMMUNITY-LICENSE-1.1.txt
externalDocs:
  description: Find out more
  url: https://docs.camunda.io/docs/apis-tools/zeebe-api-rest/overview/

servers:
  - url: "{schema}://{host}:{port}/api/v1"
    variables:
      host:
        default: localhost
        description: The hostname of a Zeebe Gateway.
      port:
        default: "8080"
        description: The port of the Zeebe REST API server.
      schema:
        default: http
        description: The schema of the Zeebe REST API server.

paths:
  /topology:
    get:
      summary: Get cluster topology.
      description: Obtains the current topology of the cluster the gateway is part of.
      responses:
        '200':
          $ref: "#/components/responses/TopologyResponse"
  /user-tasks/{userTaskKey}/completion:
    post:
      summary: Complete a user task.
      description: Completes a user task with the given key.
      parameters:
        - name: userTaskKey
          in: path
          required: true
          description: The key of the user task to complete.
          schema:
            type: integer
            format: int64
      requestBody:
        required: false
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/UserTaskCompletionRequest"

      responses:
        '204':
          description: The user task was completed successfully.
        '404':
          description: The user task with the given key was not found.
        '409':
          description: >
            The user task with the given key is in the wrong state currently.
            More details are provided in the response body.
          $ref: "#/components/responses/ProblemResponse"
        '400':
          description: >
            The user task with the given key cannot be completed.
            More details are provided in the response body.
          $ref: "#/components/responses/ProblemResponse"


components:
  responses:
    TopologyResponse:
      description: Obtains the current topology of the cluster the gateway is part of.
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/TopologyResponse"
    ProblemResponse:
      description: Response for exceptional uses cases, providing more details.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/ProblemDetail"

  schemas:
    TopologyResponse:
      description: The response of a topology request.
      type: object
      properties:
        brokers:
          description: A list of brokers that are part of this cluster.
          type: array
          nullable: true
          items:
            $ref: "#/components/schemas/BrokerInfo"
        clusterSize:
          description: The number of brokers in the cluster.
          type: integer
          format: int32
          nullable: true
        partitionsCount:
          description: The number of partitions are spread across the cluster.
          type: integer
          format: int32
          nullable: true
        replicationFactor:
          description: The configured replication factor for this cluster.
          type: integer
          format: int32
          nullable: true
        gatewayVersion:
          description: The version of the Zeebe Gateway.
          type: string
          nullable: true
    BrokerInfo:
      description: Provides information on a broker node.
      type: object
      properties:
        nodeId:
          description: The unique (within a cluster) node ID for the broker.
          type: integer
          format: int32
        host:
          description: The hostname for reaching the broker.
          type: string
        port:
          description: The port for reaching the broker.
          type: integer
          format: int32
        partitions:
          description: A list of partitions managed or replicated on this broker.
          type: array
          items:
            $ref: "#/components/schemas/Partition"
        version:
          description: The broker version.
          type: string
    Partition:
      description: Provides information on a partition within a broker node.
      type: object
      properties:
        partitionId:
          description: The unique ID of this partition.
          type: integer
          format: int32
        role:
          description: Describes the Raft role of the broker for a given partition.
          type: string
          enum:
            - leader
            - follower
            - inactive
        health:
          description: Describes the current health of the partition.
          type: string
          enum:
            - healthy
            - unhealthy
            - dead
    UserTaskCompletionRequest:
      type: object
      properties:
        variables:
          description: The variables to complete the user task with.
          type: object
          nullable: true
          $ref: "#/components/schemas/Variables"
        action:
          description: >
            A custom action value that will be accessible from user task events resulting
            from this endpoint invocation. If not provided, it will default to "complete".
          type: string
          nullable: true
    Variables:
      description: A map of variables.
      type: object
      additionalProperties: true
    ProblemDetail:
      description: >
        A Problem detail object as described in [RFC 9457](https://www.rfc-editor.org/rfc/rfc9457).
        There may be additional properties specific to the problem type.
      type: object
      properties:
        type:
          type: string
          format: uri
          description: A URI identifying the problem type.
          default: about:blank
        title:
          type: string
          description: A summary of the problem type.
        status:
          type: integer
          format: int32
          description: The HTTP status code for this problem.
          minimum: 400
          maximum: 600
        detail:
          type: string
          description: An explanation of the problem in more detail.
        instance:
          type: string
          format: uri
          description: A URI identifying the origin of the problem.
